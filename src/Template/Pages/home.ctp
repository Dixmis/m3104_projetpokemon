<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

$this->layout = false;
$cakeDescription = 'PokeCenter';
$dresseurs = TableRegistry::getTableLocator()->get('Dresseurs');
$pokemons = TableRegistry::getTableLocator()->get('Pokes');
$dresseurpokes = TableRegistry::getTableLocator()->get('DresseurPokes');
$fights = TableRegistry::getTableLocator()->get('Fights');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('home.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body class="home">

<header class="row">
    <div class="header-image"><?= $this->Html->image('pokeball-seeklogo.com.ai') ?></div>
    <div class="header-title">
        <h1>Bienvenue au Centre Pokémon !</h1>
    </div>
</header>

<div class="row">
    <div class="columns large-12">
        <div class="ctp-warning alert text-center">
            <p>Nous vous signalons que le site du Centre Pokémon traverse actuellement une période de maintenance. Retrouver nous vite sur place. L'infirmière Joëlle :)</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="columns large-6">
        <h4>Nos meilleurs dresseurs !</h4>
        <ul>
            <?php $listDresseurs = $dresseurs->find()->select(['nom', 'prenom'])->toList();
            foreach ($listDresseurs as $dresseur) {
                echo "<li>${dresseur['nom']} ${dresseur['prenom']}</li>";
            } ?>
        </ul>
    </div>
    <div class="columns large-6">
        <h4>Leurs fidèles Pokémons</h4>
        <ul>
            <?php $listPokemons = $pokemons->find()->select(['name'])/*->where(['id' => $poke_id])/**/->toList();
            foreach ($listPokemons as $pokemon) {
                echo "<li>${pokemon['name']}</li>";
            } ?>
        </ul>
    </div>
    <hr />
</div>

<div class="row">
    <div class="columns large-4">
        <h4>Les derniers combats</h4>
        <ul>
        </ul>
    </div>
    <div class="columns large-4">
        <ul>
        </ul>
    </div>
    <div class="columns large-4">
        <ul>
        </ul>
    </div>
    <hr />
</div>

<div class="row">
    <div class="columns large-12 text-center">
        <h3 class="more">À propos du Centre Pokémon</h3>
        <p>
            Nous sommes à votre service 24h/24 et 7j/7 depuis 1996 !<br />
            Retrouvez nous près de chez vous comme à Kanto, Johto ou même plus récemment à Galar :)
        </p>
    </div>
    <hr/>
</div>

</body>
</html>
